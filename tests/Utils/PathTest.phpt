<?php


use Pipaslot\Utils\Path;
use Tester\Assert;
use Tester\TestCase;

require_once __DIR__ . '/../bootstrap.php';

/**
 * @author Petr Štipek <p.stipek@email.cz>
 */
class PathTest extends TestCase
{

	public function testNormalize()
	{
		//Slashes
		Assert::equal("C:/my/path", Path::normalize("C:/my\\path/"));
		Assert::equal("/my/path", Path::normalize("\\my\\path/"));
		Assert::equal("/my/path", Path::normalize("/my/path"));
		Assert::equal("my/path", Path::normalize("my\\path/"));
		Assert::equal("my/path", Path::normalize("my\\\\path/"));
		Assert::equal("my/path", Path::normalize("my//path/"));

		//Back calling
		Assert::equal("my/path", Path::normalize("my/some/../path"));
		Assert::equal("my/some/path", Path::normalize("my/some/./path"));
		Assert::equal("", Path::normalize(""));
		Assert::equal("/", Path::normalize("/"));
	}

	public function testDirectory()
	{
		Assert::equal("some", Path::directory("my/some/path"));
		Assert::equal("some", Path::directory("my/some/file.json"));
		Assert::null(Path::directory("my"));
		Assert::null(Path::directory(""));
	}

	public function testDirectoryPath()
	{
		Assert::equal("my/some", Path::directoryPath("my/some/path"));
		Assert::equal("my/some", Path::directoryPath("my/some/file.json"));
		Assert::null(Path::directoryPath("my"));
		Assert::null(Path::directoryPath(""));
	}

	public function testName()
	{
		Assert::equal("path", Path::name("my/some/path"));
		Assert::equal("file.json", Path::name("my/some/file.json"));
		Assert::equal("file", Path::name("my/some/file.json", false));
		Assert::equal("my", Path::name("my"));
		Assert::null(Path::name(""));
	}

	public function testExtension()
	{
		Assert::null(Path::extension("my/some/path"));
		Assert::null(Path::extension("my/some/path", true));
		Assert::null(Path::extension("my"));
		Assert::null(Path::extension(""));
		Assert::equal("json", Path::extension("my/some/file.json"));
		Assert::equal(".json", Path::extension("my/some/file.json", true));
		Assert::equal("json", Path::extension(".json"));
		Assert::equal(".json", Path::extension(".json", true));
	}

	public function testDifference()
	{
		Assert::equal("/address/picture.jpg", Path::difference("some/url/address/picture.jpg", "some/url"));
	}

	public function testCopy()
	{
		Assert::exception(function () {
			Path::copy("undefinedPath", "undefinedPath");
		}, \OutOfRangeException::class);
		$fileContent = date("d.m.Y H:i:s");
		$source = TEMP_DIR . '/source.' . time() . '.txt';
		$target = TEMP_DIR . '/target.' . time() . '.txt';

		if (is_file($target)) unlink($target);
		file_put_contents($source, $fileContent);
		Path::copy($source, $target);

		Assert::true(is_file($target));
		Assert::equal($fileContent, file_get_contents($target));
		unlink($source);
		unlink($target);
	}

	public function testCopyContent()
	{
		$sourceDir = TEMP_DIR . '/source-' . time();
		$targetDir = TEMP_DIR . '/target-' . time();
		mkdir($sourceDir);
		mkdir($targetDir);

		$fileContent1 = "1-" . date("d.m.Y H:i:s");
		$fileContent2 = "2-" . date("d.m.Y H:i:s");
		$source1 = $sourceDir . '/file1.txt';
		$source2 = $sourceDir . '/file2.txt';
		$target1 = $targetDir . '/file1.txt';
		$target2 = $targetDir . '/file2.txt';
		file_put_contents($source1, $fileContent1);
		file_put_contents($source2, $fileContent2);
		if (is_file($target1)) unlink($target1);
		if (is_file($target2)) unlink($target2);

		Assert::exception(function () {
			Path::copyContent("undefinedPath", "undefinedPath");
		}, \OutOfRangeException::class);
		Assert::exception(function () use ($sourceDir) {
			Path::copyContent($sourceDir, "undefinedPath");
		}, \OutOfRangeException::class);
		Assert::exception(function () use ($targetDir) {
			Path::copyContent("undefinedPath", $targetDir);
		}, \OutOfRangeException::class);
		Assert::true(Path::copyContent($sourceDir, $targetDir));

		Assert::true(is_file($target1));
		Assert::equal($fileContent1, file_get_contents($target1));
		Assert::true(is_file($target2));
		Assert::equal($fileContent2, file_get_contents($target2));

		unlink($source1);
		unlink($source2);
		unlink($target1);
		unlink($target2);
		rmdir($sourceDir);
		rmdir($targetDir);
	}

	public function testRemove()
	{
		$dir = TEMP_DIR . '/remove-' . time();
		@mkdir($dir);
		$file = TEMP_DIR . '/remove.txt';
		file_put_contents($file, "content");
		$subDir = $dir . '/sub-remove' . time();
		@mkdir($subDir);
		$subFile = $dir . '/sub-remove.txt';
		file_put_contents($subFile, "content");

		Assert::true(Path::remove($file));
		Assert::true(Path::remove($dir));

		if (is_dir($dir)) Assert::fail("Directory $dir must not be removed");
		if (is_file($file)) Assert::fail("File $file must not be removed");
	}

	public function testRemove_withIgnored()
	{
		$dir = TEMP_DIR . '/removeWithIgnored-' . time();
		@mkdir($dir);
		$file1 = $dir . '/.no-remove.txt';
		file_put_contents($file1, "content");
		$file2 = $dir . '/remove.txt';
		file_put_contents($file2, "content");

		Assert::false(Path::remove($dir, true));

		if (!is_dir($dir)) Assert::fail("Directory $dir was removed, but must not be");
		if (!is_file($file1)) Assert::fail("File $file1 was removed, but must not be");
		if (is_file($file2)) Assert::fail("File $file2 was not be removed");

		unlink($file1);
		rmdir($dir);
	}

	public function testStringMask_remove_withMask()
	{
		$dir = TEMP_DIR . '/removeWithMask-' . time();
		@mkdir($dir);
		$file1 = $dir . '/do-not-remove.txt';
		file_put_contents($file1, "content");
		$file2 = $dir . '/remove.txt';
		file_put_contents($file2, "content");

		Assert::false(Path::remove($dir, "do-not-*"));

		if (!is_dir($dir)) Assert::fail("Directory $dir was removed, but must not be");
		if (!is_file($file1)) Assert::fail("File $file1 was removed, but must not be");
		if (is_file($file2)) Assert::fail("File $file2 must not be removed");

		Assert::true(Path::remove($dir));
		Assert::false(is_dir($file1));
		Assert::false(is_dir($dir));
	}

	public function testEmptyDirectory()
	{
		$dir = TEMP_DIR . '/emptyDirectory-' . time();
		@mkdir($dir);
		$subDir = $dir . '/sub-remove' . time();
		@mkdir($subDir);
		$subFile = $dir . '/sub-remove.txt';
		file_put_contents($subFile, "content");

		Assert::true(Path::emptyDirectory($dir));

		if (is_dir($subDir)) Assert::fail("Directory $subDir must not be removed");
		if (is_file($subFile)) Assert::fail("File $subFile must not be removed");

		rmdir($dir);
	}

	public function testEmptyDirectory_withIgnored()
	{
		$dir = TEMP_DIR . '/emptyDirectoryWithIgnored-' . time();
		@mkdir($dir);
		$file1 = $dir . '/.no-remove.txt';
		file_put_contents($file1, "content");
		$file2 = $dir . '/remove.txt';
		file_put_contents($file2, "content");

		Assert::false(Path::emptyDirectory($dir, true));

		if (!is_dir($dir)) Assert::fail("Directory $dir was removed, but must not be");
		if (!is_file($file1)) Assert::fail("File $file1 was removed, but must not be");
		if (is_file($file2)) Assert::fail("File $file2 must not be removed");

		unlink($file1);
		rmdir($dir);
	}

	public function testStringMask_emptyDirectory_withMask()
	{
		$dir = TEMP_DIR . '/emptyDirectoryWithMask-' . time();
		@mkdir($dir);
		$file1 = $dir . '/do-not-remove.txt';
		file_put_contents($file1, "content");
		$file2 = $dir . '/remove.txt';
		file_put_contents($file2, "content");

		Assert::false(Path::emptyDirectory($dir, "do-not-*"));

		if (!is_dir($dir)) Assert::fail("Directory $dir was removed, but must not be");
		if (!is_file($file1)) Assert::fail("File $file1 was removed, but must not be");
		if (is_file($file2)) Assert::fail("File $file2 must not be removed");
		unlink($file1);
		rmdir($dir);
	}

	public function testChmod()
	{
		Assert::exception(function () {
			Path::chmod("undefinedPath");
		}, \OutOfRangeException::class);

		/*$file = TEMP_DIR . '/chmod.txt';
		if (is_file($file)) unlink($file);
		file_put_contents($file, "content");

		Assert::notEqual(755, decoct(fileperms($file) & 0777));
		Path::chmod($file, 0755);
		Assert::equal(755, decoct(fileperms($file) & 0777));

		unlink($file);*/

	}

	public function testChown()
	{
		Assert::exception(function () {
			Path::chown("undefinedPath", "user");
		}, \OutOfRangeException::class);
		/*$owner = 102834;
		$file = TEMP_DIR . '/chown.txt';
		if (is_file($file)) unlink($file);
		file_put_contents($file, "content");

		Assert::notEqual($owner, fileowner($file));
		Path::chown($file, $owner);
		Assert::equal($owner, fileowner($file));

		unlink($file);*/
	}

	public function testChgrp()
	{
		Assert::exception(function () {
			Path::chgrp("undefinedPath", "group");
		}, \OutOfRangeException::class);

		/*$group = "mygroup";
		$file = TEMP_DIR . '/chgrp.txt';
		if (is_file($file)) unlink($file);
		file_put_contents($file, "content");

		Assert::notEqual($group, filegroup($file));
		Path::chgrp($file, $group);
		Assert::equal($group, filegroup($file));

		unlink($file);*/
	}

	public function testFilesAndDirectories()
	{
		$root = TEMP_DIR . '/numberOfFiles-' . time();
		@mkdir($root);
		$dir1 = $root . '/sub-numberOfFiles';
		@mkdir($dir1);
		$dir2 = $root . '/sub-numberOfFiles2';
		@mkdir($dir2);
		$dir1dir = $dir1 . '/sub-numberOfFiles';
		mkdir($dir1dir);
		$rootFile1 = $root . '/sub-numberOfFiles1.txt';
		file_put_contents($rootFile1, "content");
		$rootFile2 = $root . '/sub-numberOfFiles2.txt';
		file_put_contents($rootFile2, "content");
		$rootFile3 = $root . '/sub-numberOfFiles3.txt';
		file_put_contents($rootFile3, "content");
		$dir1dirFile = $dir1dir . '/sub-numberOfFiles1.txt';
		file_put_contents($dir1dirFile, "content");

		Assert::equal(3, Path::numberOfFiles($root));
		Assert::equal(4, Path::numberOfFiles($root, true));
		Assert::equal(2, Path::numberOfDirectories($root));
		Assert::equal(3, Path::numberOfDirectories($root, true));

		unlink($dir1dirFile);
		unlink($rootFile1);
		unlink($rootFile2);
		unlink($rootFile3);
		rmdir($dir1dir);
		rmdir($dir1);
		rmdir($dir2);
		rmdir($root);
	}

	public function testClean()
	{
		$basePath = TEMP_DIR . "/cleanTest-" . time();
		$dirs = array(
			$basePath,
			$basePath . "/dir",
			$basePath . "/dir/file.txt",
			$basePath . "/dir/subsub"
		);
		foreach ($dirs as $id => $dir) {
			//create file or directory
			if ($id == 2) file_put_contents($dir, $dir);
			else mkdir($dir);
		}
		// try clean but there is file
		Assert::false(Path::clean($basePath));
		Assert::true(file_exists($dirs[1]));
		Assert::true(file_exists($dirs[2]));
		Assert::false(file_exists($dirs[3]));
		//remove file and try it again
		unlink($dirs[2]);
		//clean but no remove self
		Assert::true(Path::clean($basePath, false));
		Assert::true(file_exists($dirs[0]));
		unset($dirs[0]);
		foreach ($dirs as $dir) {
			Assert::false(file_exists($dir));
		}
		//clean and remove path if is empty
		Assert::true(Path::clean($basePath));
		Assert::false(file_exists($basePath));
	}

	public function testAbsolutePaths_cleanPath()
	{
		$basePath = TEMP_DIR . "/cleanPathTest-" . time();
		$dirs = array(
			$basePath,
			$basePath . "/dir",
			$basePath . "/dir/file.txt",
			$basePath . "/dir/subsub",
			$basePath . "/dir/subsub/extra-sub",
			$basePath . "/dir/subsub2"
		);
		foreach ($dirs as $id => $dir) {
			//create file or directory
			if ($id == 2) file_put_contents($dir, $dir);
			else mkdir($dir);
		}
		Assert::false(Path::cleanPath($basePath, $dirs[4]));
		Assert::true(file_exists($dirs[5]));
		Assert::false(file_exists($dirs[4]));
		Assert::false(file_exists($dirs[3]));
		Assert::true(file_exists($dirs[2]));
		Assert::true(file_exists($dirs[1]));
		Assert::true(file_exists($dirs[0]));

		unlink($dirs[2]);
		Assert::true(Path::cleanPath($basePath, $dirs[5]));
		Assert::false(file_exists($dirs[5]));
		Assert::false(file_exists($dirs[1]));
		Assert::true(file_exists($dirs[0]));

		Assert::true(Path::cleanPath($basePath, $dirs[5], true));
		Assert::false(file_exists($dirs[0]));
	}

	public function testRelativePaths_cleanPath()
	{
		$basePath = TEMP_DIR . "/cleanPathTest2-" . time();
		$dirs = array(
			$basePath,
			$basePath . "/dir",
			$basePath . "/dir/file.txt",
			$basePath . "/dir/subsub",
			$basePath . "/dir/subsub/extra-sub",
			$basePath . "/dir/subsub2"
		);
		foreach ($dirs as $id => $dir) {
			//create file or directory
			if ($id == 2) file_put_contents($dir, $dir);
			else mkdir($dir);
		}
		Assert::false(Path::cleanPath($basePath, "dir/subsub/extra-sub"));
		Assert::true(file_exists($dirs[5]));
		Assert::false(file_exists($dirs[4]));
		Assert::false(file_exists($dirs[3]));
		Assert::true(file_exists($dirs[2]));
		Assert::true(file_exists($dirs[1]));
		Assert::true(file_exists($dirs[0]));

		unlink($dirs[2]);
		Assert::true(Path::cleanPath($basePath, "dir/subsub2"));
		Assert::false(file_exists($dirs[5]));
		Assert::false(file_exists($dirs[1]));
		Assert::true(file_exists($dirs[0]));

		Assert::true(Path::cleanPath($basePath, "dir/subsub2", true));
		Assert::false(file_exists($dirs[0]));
	}
}

$test = new PathTest();
$test->run();