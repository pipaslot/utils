<?php
namespace Pipaslot\Utils;

use Tester\Assert;
use Tester\TestCase;

require_once __DIR__ . '/../bootstrap.php';

/**
 * @author Petr Štipek <p.stipek@email.cz>
 */
class WwwDirectoryTest extends TestCase
{
	const EXISTING = 'existing';

	public function setUp()
	{
		parent::setUp();
		$_SERVER['REQUEST_URI'] = '/my-local/address';
		$_SERVER['SCRIPT_FILENAME'] = __FILE__;
	}

	public function test_noArguments_shouldUseServerVariable()
	{
		$dir = "C:\\" . self::EXISTING . '/sub-dir';

		$_SERVER['REQUEST_URI'] = '/';
		$_SERVER['SCRIPT_FILENAME'] = $dir . '/index.php';
		$www = new WwwDirectory();

		Assert::equal(Path::normalize($dir . '/'), $www->getAbsolute());
		Assert::equal("/", $www->getRelative());
	}

	public function test_notExistingPath()
	{
		Assert::exception(function () {
			new WwwDirectory("non-exists");
		}, \OutOfRangeException::class);
	}

	public function test_relativePath_shouldUseServerVariableCombinedWithRelativePath()
	{
		$dir = "C:\\" . self::EXISTING . '/sub-dir';

		$_SERVER['REQUEST_URI'] = '/';
		$_SERVER['SCRIPT_FILENAME'] = $dir . '/index.php';
		$relative = "my-extra-path";
		$www = new WwwDirectory($relative);

		Assert::equal(Path::normalize($dir . '/' . $relative), $www->getAbsolute());
		Assert::equal("/" . $relative, $www->getRelative());
	}

	public function test_getFilePath()
	{
		$dir = new WwwDirectory(self::EXISTING);
		$file = 'myFile.jpg';
		$expect = self::EXISTING . '/myFile.jpg';
		Assert::true($this->endsWith($dir->getAbsolute($file), $expect),"Path: ".$dir->getAbsolute($file)." does not ends with: $expect");
		Assert::true($this->endsWith($dir->getRelative($file), $expect),"Path: ".$dir->getRelative($file)." does not ends with: $expect");
	}
	public function test_getFilePath2()
	{
		$dir = new WwwDirectory();
		$file = 'myFile.jpg';
		$expect = '/myFile.jpg';
		Assert::equal($expect,$dir->getRelative($file));
	}

	private function endsWith($haystack, $needle)
	{
		return strlen($needle) === 0 || substr($haystack, -strlen($needle)) === $needle;
	}

	public function test_missingServerParams_suggestExplicitRootException()
	{
		unset($_SERVER['REQUEST_URI']);
		unset($_SERVER['SCRIPT_FILENAME']);
		Assert::exception(function () {
			new WwwDirectory(self::EXISTING);
		}, \DomainException::class);

	}

	public function test_explicitRoot()
	{
		$root = "C:\\" . self::EXISTING . '/sub-dir';
		$relative = "my-extra-path";

		$www = new WwwDirectory($relative, $root);

		Assert::equal(Path::normalize($root . '/' . $relative), $www->getAbsolute());
		Assert::equal("/" . $relative, $www->getRelative());
	}

	/*********************************** Server variable suggestion **************************/

	public function test_runFromDomainWithRoute_URIisDifferen_shouldReturnEmptyRelativePath()
	{
		$_SERVER['REQUEST_URI'] = "/account/";
		$_SERVER['SCRIPT_FILENAME'] = 'C:/www/' . self::EXISTING . '/www/index.php';
		$www = new WwwDirectory();

		Assert::equal(Path::directoryPath($_SERVER['SCRIPT_FILENAME']), $www->getAbsolute());
		Assert::equal("/", $www->getRelative());
	}

	public function test_runFromDomainWithoutRoute_URIisDifferen_shouldReturnEmptyRelativePath()
	{
		$_SERVER['REQUEST_URI'] = "/";
		$_SERVER['SCRIPT_FILENAME'] = 'C:/www/' . self::EXISTING . '/www/index.php';
		$www = new WwwDirectory();

		Assert::equal(Path::directoryPath($_SERVER['SCRIPT_FILENAME']), $www->getAbsolute());
		Assert::equal("/", $www->getRelative());
	}

	public function test_runFromFolderWithoutRoute_URIisDifferen_shouldReturnBaseRelativePath()
	{
		$_SERVER['REQUEST_URI'] = "/" . self::EXISTING . "/www";
		$_SERVER['SCRIPT_FILENAME'] = 'C:/www/' . self::EXISTING . '/www/index.php';
		$www = new WwwDirectory();

		Assert::equal(Path::directoryPath($_SERVER['SCRIPT_FILENAME']), $www->getAbsolute());
		Assert::equal($_SERVER['REQUEST_URI'], $www->getRelative());
	}

	public function test_runFromFolderWithRoute_URIisDifferen_shouldReturnBaseRelativePath()
	{
		$basePath = "/" . self::EXISTING . "/www";
		$_SERVER['REQUEST_URI'] = $basePath . "/account/";
		$_SERVER['SCRIPT_FILENAME'] = 'C:/www/' . self::EXISTING . '/www/index.php';
		$www = new WwwDirectory();

		Assert::equal(Path::directoryPath($_SERVER['SCRIPT_FILENAME']), $www->getAbsolute());
		Assert::equal($basePath, $www->getRelative());
	}

	public function test_runFromFolderWithRouteAndRepeatedDirectory_URIisDifferen_shouldReturnBaseRelativePath()
	{
		$repeat = '/' . self::EXISTING . "/www";
		$basePath = $repeat . $repeat;
		$_SERVER['REQUEST_URI'] = $basePath . "/account/";
		$_SERVER['SCRIPT_FILENAME'] = 'C:/www/' . self::EXISTING . '/www/index.php';
		$www = new WwwDirectory();

		Assert::equal(Path::directoryPath($_SERVER['SCRIPT_FILENAME']), $www->getAbsolute());
		Assert::equal($repeat, $www->getRelative());
	}

	public function test_requestUriWithoutFirstSlash()
	{
		$_SERVER['REQUEST_URI'] = 'my-local/address';
		$_SERVER['SCRIPT_FILENAME'] = 'C:/www/' . self::EXISTING . '/www/' . $_SERVER['REQUEST_URI'] . '/index.php';
		$dir = new WwwDirectory();
		Assert::equal('/' . $_SERVER['REQUEST_URI'], $dir->getRelative());
	}

}

function is_dir($filename)
{
	if (strpos($filename, WwwDirectoryTest::EXISTING) !== FALSE) return true;
	return \is_dir($filename);
}

$test = new WwwDirectoryTest();
$test->run();

