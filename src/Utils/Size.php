<?php

namespace Pipaslot\Utils;

/**
 * Static class counting the size of the file and directories. It includes conversion feature
 *
 * @author Petr Štipek <p.stipek@email.cz>
 */
class Size
{
	const B = 'B',
		KB = 'KB',
		MB = 'MB',
		GB = 'GB',
		TB = 'TB',
		PB = 'PB';

	/**
	 * Calculate size of files or directories
	 * @param string $path
	 * @return int
	 */
	public static function calculate($path)
	{
		if (!file_exists($path)) throw new \OutOfRangeException("File or path $path does not exist");
		if (is_dir($path)) {
			$size = 0;
			foreach (scandir($path) as $item) {
				if ($item == '.' || $item == '..') continue;
				$size += self::calculate($path . DIRECTORY_SEPARATOR . $item);
			}
			return $size;
		}
		return filesize($path);
	}

	/**
	 * Converts size on the specified unit
	 * @param int $size
	 * @param string $unit
	 * @param int $precision
	 * @return float
	 * @throws \OutOfRangeException
	 */
	public static function convert($size, $unit = self::B, $precision = 0)
	{
		$steps = array(self::B => 0, self::KB => 1, self::MB => 2, self::GB => 3, self::TB => 4, self::PB => 5);
		if (!$steps[$unit]) {
			throw new \OutOfRangeException("Unit '$unit' does not defined in [" . implode(', ', array_keys($steps)) . "]");
		}
		for ($i = 0; $i <= $steps[$unit]; $i++) {
			$size /= 1000;
		}
		return round($size, (int)$precision);
	}

	/**
	 * Converting an automatic search unit
	 * Converts size with the accuracy of a number less than one thousand units and adds
	 * @param int $size
	 * @param int $precision
	 * @return string
	 */
	public static function convertAuto($size, $precision = 0)
	{
		$unit = array(0 => self::B, 1 => self::KB, 2 => self::MB, 3 => self::GB, 4 => self::TB, 5 => self::PB);
		$i = 0;
		while ($size >= 1000) {
			$size /= 1000;
			$i++;
		}
		return round($size, $precision) . $unit[$i];
	}

}