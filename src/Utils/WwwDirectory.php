<?php

namespace Pipaslot\Utils;

/**
 * @author Petr Štipek <p.stipek@email.cz>
 */
class WwwDirectory
{
	private $relative;
	private $absolute;

	/**
	 * TempDirectory constructor.
	 * @param string $relative
	 * @param null|string $webRoot Explicit path to web root, other it takes path from $_SERVER
	 */
	public function __construct($relative = "", $webRoot = null)
	{
		if (!$webRoot) {
			$webRoot = $this->getRootPath();
		}
		$this->absolute = Path::normalize($webRoot . '/' . $relative);
		$this->relative = '/' . ltrim(Path::normalize($this->getBasePath() . '/' . $relative), '/');
		if (!is_dir($this->absolute)) {
			throw new \OutOfRangeException("Directory $this->absolute does not exist.");
		}
	}

	/**
	 * Absolute path to public temp directory
	 * @param null|string $fileName
	 * @return string
	 */
	public function getAbsolute($fileName = null)
	{
		return $this->absolute . (empty($fileName) ? '' : '/' . $fileName);
	}

	/**
	 * Relative path to public temp directory
	 * @param null|string $fileName
	 * @return string
	 */
	public function getRelative($fileName = null)
	{
		return Path::normalize($this->relative . (empty($fileName) ? '' : '/' . $fileName));
	}

	/**
	 * Web root path
	 * @return string|bool
	 */
	private function getRootPath()
	{
		if (!isset($_SERVER['SCRIPT_FILENAME'])) {
			throw new \DomainException("Missing \$_SERVER parameters. Use explicit web root.");
		}
		return Path::directoryPath($_SERVER['SCRIPT_FILENAME']);
	}

	/**
	 * Return relative base path to root directory
	 * @return string
	 */
	private function getBasePath()
	{
		if (!isset($_SERVER['REQUEST_URI'])) {
			return '';
		}
		$path = $_SERVER['REQUEST_URI'];

		$pos = strrpos($path, '/');
		$relative = $pos === FALSE ? '' : substr($path, 0, $pos + 1);

		$dir = Path::directoryPath($_SERVER['SCRIPT_FILENAME']);
		$final = $this->intersect($dir, $relative);
		if (substr($final, 0, 1) !== '/') {
			return '/' . $final;
		}
		return $final;
	}

	/**
	 * @param string $absolute Normalized path
	 * @param string $relative Normalized path
	 * @return string
	 */
	private function intersect($absolute, $relative)
	{
		$abs = trim($absolute, "/");
		$rel = trim($relative, "/");
		$relArray = explode("/", $rel);
		while (count($relArray) > 0) {
			$imp = implode("/", $relArray);
			if (empty($imp)) {
				break;
			}
			$pos = strrpos($abs, $imp);
			if ($pos !== false) {
				return substr($absolute, $pos);
			}
			array_pop($relArray);
		}
		return '/';

	}

}