<?php

namespace DNweb\Utils;

/**
 * Utils for IpAddresses
 *
 * @author Petr Štipek <p.stipek@email.cz>
 */
class IpAddress
{

	const REGEXP = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

	/*	 * *************************** Převody ***************************** */

	/**
	 * Converts IP to unsigned int
	 * @param string $ip
	 * @return int
	 */
	public static function ipToInt($ip)
	{
		$long = ip2long($ip);
		return (int)($long < 0 ? $long + 4294967296 : $long);
	}

	/**
	 * Converts unsigned int to IP
	 * @param int $int
	 * @return string
	 */
	public static function intToIp($int)
	{
		$long = $int > 2147483648 ? $int - 4294967296 : $int;
		return long2ip($long);
	}

	/**
	 * Convert IP and mask to CIDR
	 * @example addressToCidr("127.0.0.1","255.255.255.128"); return: "127.0.0.1/25"
	 * @param string $ip
	 * @param string $netmask
	 * @return string
	 */
	public static function addressToCidr($ip, $netmask)
	{
		return $ip . "/" . self::netmask2cidr($netmask);
	}

	/**
	 * Return CIDR suffix by netmask
	 * @example netmask2cidr("255.255.255.128"); return: 25;
	 * @param string $netmask
	 * @return int
	 */
	public static function netmask2cidr($netmask)
	{
		$cidr = 0;
		foreach (explode('.', $netmask) as $number) {
			for (; $number > 0; $number = ($number << 1) % 256) {
				$cidr++;
			}
		}
		return $cidr;
	}

	/**
	 * Převede Cidr na adresu a masku
	 * @example cidrToAddress("127.0.0.1/25"); return: array("127.0.0.1","255.255.255.128");
	 * @param string $cidr
	 * @return array
	 * @throws \OutOfRangeException
	 */
	public static function cidrToAddress($cidr)
	{
		$e = explode("/", $cidr);
		if (count($e) !== 2)
			throw new \OutOfRangeException("CIDR expected value into format 'xxx.xxx.xxx.xxx/xx', '$cidr' given'.");

		return array($e[0], self::cidrSuffixToMask($e[1]));
	}

	/**
	 * @param $cidr
	 * @return string
	 */
	public static function cidrToIp($cidr)
	{
		$e = explode("/", $cidr);
		if (count($e) !== 2)
			throw new \OutOfRangeException("CIDR expected value into format 'xxx.xxx.xxx.xxx/xx', '$cidr' given'.");
		return $e[0];
	}

	/**
	 * @param $cidr
	 * @return string
	 * @throws \OutOfRangeException
	 */
	public static function cidrToMask($cidr)
	{
		$e = explode("/", $cidr);
		if (count($e) !== 2)
			throw new \OutOfRangeException("CIDR expected value into format 'xxx.xxx.xxx.xxx/xx', '$cidr' given'.");
		return self::cidrSuffixToMask($e[1]);
	}

	/**
	 * Coverts CIDR extension to mask
	 * @example cidrSuffixToMask("25); return: "255.255.255.128"
	 * @param int $bitCount
	 * @return string
	 */
	public static function cidrSuffixToMask($bitCount)
	{
		$netmask = str_split(str_pad(str_pad('', (int)$bitCount, '1'), 32, '0'), 8);
		foreach ($netmask as &$element) {
			$element = bindec($element);
		}
		return implode('.', $netmask);
	}

	/**
	 * Number of IP addresses order by CIDR extension
	 * @example getNumberOfAddressesByCidrSuffix("127.0.0.1/28") return 16;
	 * @param string $cidr
	 * @return int
	 * @throws \OutOfRangeException
	 */
	public static function getNumberOfAddressesByCidr($cidr)
	{
		$e = explode("/", $cidr);
		if (count($e) !== 2)
			throw new \OutOfRangeException("CIDR expected value into format 'xxx.xxx.xxx.xxx/xx', '$cidr' given'.");
		return self::getNumberOfAddressesByCidrSuffix($e[1]);
	}

	/**
	 * Number of IP addresses order by CIDR suffix
	 * @example getNumberOfAddressesByCidrSuffix(28) return 16;
	 * @param int $bitCount
	 * @return int
	 */
	public static function getNumberOfAddressesByCidrSuffix($bitCount)
	{
		return 1 << (32 - (int)$bitCount);
	}

	/*	 * *************************** Validations ***************************** */

	/**
	 * Is IP address valid
	 * @param string $address
	 * @return bool
	 */
	public static function isIP($address)
	{
		return (bool)ip2long($address);
	}

	/**
	 * Example: isInCidr("127.0.0.1", "127.0.0.1/32"):
	 * @param string $ip
	 * @param string $cidr
	 * @return boolean
	 */
	public static function isInCidr($ip, $cidr)
	{
		list($subnet, $mask) = explode('/', $cidr);

		if ((ip2long($ip) & ~((1 << (32 - $mask)) - 1)) == ip2long($subnet)) {
			return true;
		}
		return false;
	}

	/**
	 * @param $cidr
	 * @return bool
	 * @throws \OutOfRangeException
	 */
	public static function isCidr($cidr)
	{
		$e = explode("/", $cidr);
		if (count($e) !== 2)
			throw new \OutOfRangeException("CIDR expected value into format 'xxx.xxx.xxx.xxx/xx', '$cidr' given'.");
		list($ip, $bitcount) = $e;
		if (!self::isIP($ip)) return false;
		if ($bitcount < 0 OR $bitcount > 32) return false;
		return true;
	}

	/*	 * *************************** Generating ***************************** */

	/**
	 * Generate array of all IP addresses in range
	 * @example getRange("127.0.0.0","127.0.0.4"); return: array("127.0.0.1", "127.0.0.2", "127.0.0.3")
	 * @param string $networkAddress
	 * @param string $broadcast
	 * @return array
	 */
	public static function getRange($networkAddress, $broadcast)
	{
		$minId = ip2long($networkAddress);
		$maxId = ip2long($broadcast);
		if ($minId > $maxId) {
			$minId = $maxId;
			$maxId = ip2long($networkAddress);
		}
		$range = array();
		for ($i = $minId + 1; $i < $maxId; $i++) {
			$range[] = long2ip($i);
		}
		return $range;
	}

	/**
	 * Generate array of all IP addresses in range
	 * @example getRangeByCidr("127.0.0.0/30"); return: array("127.0.0.1", "127.0.0.2", "127.0.0.3")
	 * @param string $cidr
	 * @return array
	 */
	public static function getRangeByCidr($cidr)
	{
		$e = explode("/", $cidr);
		if (count($e) !== 2)
			throw new \OutOfRangeException("CIDR expected value into format 'xxx.xxx.xxx.xxx/xx', '$cidr' given'.");
		list($network, $bitCount) = $e;
		$broadcast = long2ip(ip2long($network) + self::getNumberOfAddressesByCidrSuffix($bitCount) - 1);
		return self::getRange($network, $broadcast);
	}

	/**
	 * Get client ip address from web server variables
	 * @return string
	 */
	function getClientIp()
	{
		if (isset($_SERVER['HTTP_CLIENT_IP']))
			return $_SERVER['HTTP_CLIENT_IP'];
		else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			return $_SERVER['HTTP_X_FORWARDED_FOR'];
		else if (isset($_SERVER['HTTP_X_FORWARDED']))
			return $_SERVER['HTTP_X_FORWARDED'];
		else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
			return $_SERVER['HTTP_FORWARDED_FOR'];
		else if (isset($_SERVER['HTTP_FORWARDED']))
			return $_SERVER['HTTP_FORWARDED'];
		else if (isset($_SERVER['REMOTE_ADDR']))
			return $_SERVER['REMOTE_ADDR'];

		return 'UNKNOWN';
	}
}
