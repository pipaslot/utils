<?php

namespace Pipaslot\Utils;

/**
 * FileSystem management
 *
 * @author Petr Štipek <p.stipek@email.cz>
 */
class Path
{

	/**
	 * Messages of failing
	 * @var array
	 */
	private static $messages = array();

	/**
	 * Converts format: /etc/bin\..\user  to  /etc/user
	 * @param string $path
	 * @return string
	 */
	public static function normalize($path)
	{
		$path = str_replace('\\', '/', $path);
		if($path=="/")return $path;
		$exp = explode('/', $path);
		$valid = array();
		foreach ($exp as $i => $val) {
			if ($val === "..") {
				array_pop($valid);
			} else if ($val == null AND $i > 0) {
				//Ignore multiple slashes, but let first
			} else if ($val !== ".") {
				array_push($valid, $val);
			}
		}
		return rtrim(implode('/', $valid), '/');
	}

	/**
	 * Gets directory name title of a file, or parent directory od subdirectories
	 * @param string $path
	 * @return string|null
	 */
	public static function directory($path)
	{
		$normalizedPath = self::normalize($path);
		$ex = explode("/", $normalizedPath);
		return count($ex) >= 2 ? $ex[count($ex) - 2] : null;
	}

	/**
	 * Getting the path to file or directory parent directory
	 * @param string $path
	 * @return string|null
	 */
	public static function directoryPath($path)
	{
		$normalizedPath = self::normalize($path);
		$parentPath = substr($normalizedPath, 0, strrpos($normalizedPath, "/"));
		return empty($parentPath) ? null : $parentPath;
	}

	/**
	 * Getting a filename without path
	 * @param string $path
	 * @param bool $withExtension
	 * @return string|null
	 */
	public static function name($path, $withExtension = true)
	{
		$normalizedPath = self::normalize($path);
		$slashPosition = strrpos($normalizedPath, "/");
		if ($slashPosition === false) return empty($path) ? null : $path;
		$fullName = substr($normalizedPath, strrpos($normalizedPath, "/") + 1);
		if ($withExtension OR is_dir($normalizedPath)) return $fullName;
		$name = substr($fullName, 0, strlen($fullName) - (strlen($fullName) - strrpos($fullName, ".")));
		return empty($name) ? null : $name;
	}

	/**
	 * Gets file extension
	 * @param string $path
	 * @param bool $withDot Return extension with dot
	 * @return string
	 */
	public static function extension($path, $withDot = false)
	{
		$name = self::name($path, true);
		if (!$name) return null;

		$dotPosition = strrpos($path, ".");
		if ($dotPosition === false) return null;

		$ext = substr($path, $dotPosition + 1);
		return ($withDot ? "." : "") . $ext;
	}

	/**
	 * Returns difference roads if the string passed as a parameter to find a consensus and be returned to the address
	 * eg:
	 *        $path = "some/url/address/picture.jpg
	 *        $difference = "some/url"
	 *        result: "/address/picture.jpg
	 * @param string $path
	 * @param string $difference
	 * @return string
	 */
	public static function difference($path, $difference)
	{
		$path = self::normalize($path);
		$difference = self::normalize($difference);
		if (is_string($difference) AND strpos($path, $difference) == 0) {
			return substr($path, strlen($difference));
		}
		return $path;
	}


	/**
	 * Copy file or directory
	 * @param string $source
	 * @param string $target
	 * @return bool
	 */
	public static function copy($source, $target)
	{
		if (!file_exists($source)) throw new \OutOfRangeException("Existing file or directory was expected, $source given.");
		if (!is_dir($source)) return copy($source, $target);

		if (!mkdir($target)) return false;
		foreach (scandir($source) as $item) {
			if ($item == '.' || $item == '..') continue;
			if (!self::copy($source . DIRECTORY_SEPARATOR . $item, $target . DIRECTORY_SEPARATOR . $item)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Copies the contents of a directory to another
	 * @param string $source Source directory
	 * @param string $target Target directory
	 * @return bool
	 */
	public static function copyContent($source, $target)
	{
		if (!is_dir($source)) throw new \OutOfRangeException("Existing directory was expected, $source given.");
		if (!is_dir($target)) throw new \OutOfRangeException("Existing directory was expected, $target given.");

		foreach (scandir($source) as $item) {
			if ($item == '.' || $item == '..') continue;
			if (!self::copy($source . DIRECTORY_SEPARATOR . $item, $target . DIRECTORY_SEPARATOR . $item)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Remove recursive file and numberOfDirectories
	 * @param string $path Target file or directory
	 * @param array|string|true $ignoreMasks Ignore files order by patterns
	 * @return boolean Returns true if all were removed
	 */
	public static function remove($path, $ignoreMasks = null)
	{
		$pattern = self::buildPattern($ignoreMasks);
		return self::removeRecursion($path, $pattern);
	}

	/**
	 * Remove recursive numberOfFiles and numberOfDirectories inside directory
	 * @param string $path Target file or directory
	 * @param array|string|true $ignoreMasks Ignore files order by patterns
	 * @return bool Returns true if all were removed
	 */
	public static function emptyDirectory($path, $ignoreMasks = null)
	{
		if (!is_dir($path)) throw new \OutOfRangeException("Existing directory was expected, $path given.");
		$pattern = self::buildPattern($ignoreMasks);
		$removedAll = true;
		foreach (scandir($path) as $item) {
			if ($item == '.' || $item == '..') continue;
			if (!self::removeRecursion($path . '/' . $item, $pattern)) $removedAll = false;
		}
		return $removedAll;
	}

	/**
	 * Converts pattern to regular expression.
	 * @param  array
	 * @return string|null
	 */
	private static function buildPattern($masks)
	{
		if (!$masks) return null;
		$masks = $masks === true ? array(".*") : (is_array($masks) ? $masks : array($masks));
		$pattern = array();
		foreach ($masks as $mask) {
			$mask = rtrim(strtr($mask, '\\', '/'), '/');
			$prefix = '';
			if ($mask === '') {
				continue;

			} elseif ($mask === '*') {
				return NULL;

			} elseif ($mask[0] === '/') { // absolute fixing
				$mask = ltrim($mask, '/');
				$prefix = '(?<=^/)';
			}
			$pattern[] = $prefix . strtr(preg_quote($mask, '#'),
					array('\*\*' => '.*', '\*' => '[^/]*', '\?' => '[^/]', '\[\!' => '[^', '\[' => '[', '\]' => ']', '\-' => '-'));
		}
		return $pattern ? '#/(' . implode('|', $pattern) . ')\z#i' : NULL;
	}

	/**
	 * Remove recursive files
	 * @param $path
	 * @param array|string|true $pattern Ignored files pattern
	 * @return bool Returns true if all were removed
	 */
	private static function removeRecursion($path, $pattern)
	{
		if (!file_exists($path)) throw new \OutOfRangeException("Existing file or directory was expected, $path given.");
		if ($pattern && preg_match($pattern, strtr($path, '\\', '/'))) return false;
		if (!is_dir($path)) return unlink($path);

		$removedAll = true;
		foreach (scandir($path) as $item) {
			if ($item == '.' || $item == '..') continue;
			if (!static::removeRecursion($path . '/' . $item, $pattern)) $removedAll = false;
		}
		return @rmdir($path) === true AND $removedAll === true;
	}

	/**
	 * Change recursive file and directory attributes
	 * @param string $path
	 * @param $fileMode
	 * @param $dirMode
	 * @param array $report Files and numberOfDirectories where could not be changed attributes
	 * @return bool
	 */
	public static function chmod($path, $fileMode = 0755, $dirMode = 0644, &$report = array())
	{
		if (!file_exists($path)) throw new \OutOfRangeException("Existing file or directory was expected, $path given.");

		self::$messages = array();
		self::chmodRecursion($path, $fileMode, $dirMode);
		$report = self::$messages;
		return count(self::$messages) === 0;
	}

	/**
	 * @param string $path
	 * @param $fileMode
	 * @param $dirMode
	 */
	private static function chmodRecursion($path, $fileMode = 0755, $dirMode = 0644)
	{
		if (is_dir($path)) {
			foreach (scandir($path) as $item) {
				if ($item == '.' || $item == '..') continue;
				self::chmodRecursion($path . DIRECTORY_SEPARATOR . $item, $fileMode, $dirMode);
			}
			if (!chmod($path, $dirMode)) self::$messages[] = $path;
		} else {
			if (!chmod($path, $fileMode)) self::$messages[] = $path;
		}
	}

	/**
	 * Change recursive owner
	 * @param string $path
	 * @param string $user
	 * @param array $report Files and numberOfDirectories where could not be changed attributes
	 * @return bool
	 */
	public static function chown($path, $user, &$report = array())
	{
		if (!file_exists($path)) throw new \OutOfRangeException("Existing file or directory was expected, $path given.");

		self::$messages = array();
		self::chownRecursion($path, $user);
		$report = self::$messages;
		return count(self::$messages) === 0;
	}

	/**
	 * @param string $path
	 * @param string $user
	 */
	private static function chownRecursion($path, $user)
	{
		if (is_dir($path)) {
			foreach (scandir($path) as $item) {
				if ($item == '.' || $item == '..') continue;
				self::chownRecursion($path . DIRECTORY_SEPARATOR . $item, $user);
			}
		}
		if (!chown($path, $user)) self::$messages[] = $path;
	}

	/**
	 * Change recursive owning group
	 * @param string $path
	 * @param string $group
	 * @param array $report Files and numberOfDirectories where could not be changed attributes
	 * @return bool
	 */
	public static function chgrp($path, $group, &$report = array())
	{
		if (!file_exists($path)) throw new \OutOfRangeException("Existing file or directory was expected, $path given.");

		self::$messages = array();
		self::chgrpRecursion($path, $group);
		$report = self::$messages;
		return count(self::$messages) === 0;
	}

	/**
	 * @param string $path
	 * @param string $group
	 */
	private static function chgrpRecursion($path, $group)
	{
		if (is_dir($path)) {
			foreach (scandir($path) as $item) {
				if ($item == '.' || $item == '..') continue;
				self::chgrpRecursion($path . DIRECTORY_SEPARATOR . $item, $group);
			}
		}
		if (!chgrp($path, $group)) self::$messages[] = $path;
	}

	/**
	 * Sum number of numberOfFiles
	 * @param string $path
	 * @param bool $recursive Sum recursive
	 * @return int
	 */
	public static function numberOfFiles($path, $recursive = false)
	{
		return self::sumFilesRecursive($path, true, $recursive);
	}

	/**
	 * Sum number of numberOfDirectories
	 * @param string $path
	 * @param bool $recursive Sum recursive
	 * @return int
	 */
	public static function numberOfDirectories($path, $recursive = false)
	{
		return self::sumFilesRecursive($path, false, $recursive);
	}

	/**
	 * @param string $path
	 * @param bool $sumFiles Files or numberOfDirectories
	 * @param bool $recursive
	 * @throws \OutOfRangeException
	 * @return int
	 */
	private static function sumFilesRecursive($path, $sumFiles, $recursive = true)
	{
		if (!is_dir($path)) throw new \OutOfRangeException("Path '" . $path . "' must be directory.");
		$count = 0;
		foreach (scandir($path) as $item) {
			if ($item == '.' || $item == '..') continue;
			if (is_dir($path . DIRECTORY_SEPARATOR . $item)) {
				if (!$sumFiles) {
					$count++;
				}
				if ($recursive) {
					$count += self::sumFilesRecursive($path . DIRECTORY_SEPARATOR . $item, $sumFiles);
				}
			} else if ($sumFiles) {
				$count++;
			}
		}
		return $count;
	}

	/**
	 * Recursive remove empty directories
	 * @param string $path
	 * @param bool $removeSelf Remove passed directory if is empty or only clean sub folders
	 * @return bool Successfully cleaned
	 */
	public static function clean($path, $removeSelf = true)
	{
		if (is_dir($path)) {
			$cleaned = true;
			foreach (new \FilesystemIterator($path) as $item) {
				if (static::clean($item, true) == false) $cleaned = false;
			}
			if ($cleaned) {
				if ($removeSelf) rmdir($path);
				return true;
			}
		}
		return false;
	}

	/**
	 * Remove empty directories between defined from and to path
	 * @param string $from absolute path to start directory
	 * @param string $to absolute or relative path to end directory
	 * @param bool $removeFromDir Remove From directory if is empty
	 * @return bool Successfully cleaned
	 */
	public static function cleanPath($from, $to, $removeFromDir = false)
	{

		$nFrom = self::normalize($from);
		$nTo = self::normalize($to);
		if (strncmp($nTo, $nFrom, strlen($nFrom)) === 0) {
			$range = substr($nTo, strlen($nFrom));
		} else {
			$range = $nTo;
		}
		$dirs = explode("/", trim($range, "/"));
		$paths = array();
		$dirSum = $nFrom;
		if ($removeFromDir) $paths[] = $nFrom;
		foreach ($dirs as $dir) {
			$dirSum .= "/" . $dir;
			$paths[] = $dirSum;
		}
		$reversePath = array_reverse($paths);
		foreach ($reversePath as $path) {
			if (is_dir($path)) {
				if (self::numberOfDirectories($path) == 0 AND self::numberOfFiles($path) == 0) {
					rmdir($path);
				} else return false;
			}
		}

		return true;
	}
}
